import asideLists from './asideLists'
import experience from './experiences'
import projects from './projects'

export default [ asideLists, experience, projects ]
