export default [
  {
    name: "Contact",
    items: [
      {label: "kyle@tinyrhino.dk"},
      {label: "+61 410 085361"},
      {label: "GitLab", link: "https://gitlab.com/kjkta/"},
      {label: "LinkedIn", link: "https://www.linkedin.com/in/kyle-thomson/"},
      {label: "Instagram", link: "https://www.instagram.com/mrkylejay/"},
    ]
  },
  {
    name: "Places I've called home",
    items: [
      {label: "Copenhagen, Denmark"},
      {label: "Brevard, North Carolina, USA"},
      {label: "Rotorua, New Zealand"},
      {label: "Melbourne, Australia"},
    ]
  },
  {
    name: "Tools I use every day",
    items: [
      {label: "React JS"},
      {label: "React Native"},
      {label: "Babel"},
      {label: "ECMAScript 7"},
      {label: "NodeJS"},
      {label: "GitLab CI"},
      {label: "Webpack"},
      {label: "AWS & Azure"},
    ]
  },
  {
    name: "Things I like doing",
    items: [
      {label: "Give talks (How to make a tech choice)", link: "https://github.com/tiny-rhino/how-to-make-a-tech-choice"},
      {label: "Travel"},
      {label: "Ride bikes"},
    ]
  },
]
