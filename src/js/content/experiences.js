import {Experience} from '../Home/Experience'

export default {
  id: 'experience',
  title: 'Experience',
  component: Experience,
  items: [
    {
      position: "Co-Founder & Front-end Lead",
      company: "Tiny Rhino",
      location: "Copenhagen, Denmark",
      link: "https://tinyrhino.dk/",
      duration: "December 2016 - Present",
      description: "At Tiny Rhino we provide expert consulting for rapid Web & Mobile App solutions for startups and development teams. We specialise in building custom applications with ReactJS and Django. By producing prototypes, wireframes, UI design and front-end development, we guide our clients through each phase of their product development.",
    },
    {
      position: "Co-Founder & Front-end Lead",
      company: "Ticketbuter",
      location: "Copenhagen, Denmark",
      link: "https://ticketbutler.io",
      duration: "December 2016 - Present",
      description: "At Ticketbutler I work with a fantastic team with the vision of making ticketing accessible and easy. We improve and maintain the Ticketbutler platform in ways which best suit the needs of both our platform owners and their customers. The platform has it's own custom theming configuration which allows new ticket platforms to be launched in just hours.",
    },
    {
      position: "Partner & Front-end Lead",
      company: "BilletFix",
      location: "Copenhagen, Denmark",
      link: "https://billetfix.dk",
      duration: "February 2016 - Present",
      description: "BilletFix is the idea which Ticketbutler originated from. BilletFix is a ticketing system we developed to make alleviate the pains student organisers have with ticketing student events. The team and I developed front-end of BilletFix which features super-fast event creation and ticket purchasing. BilletFix serves ticket sales for thousands of students in Denmark every year.",
    },
    {
      position: "Head of Mountaineering",
      company: "Camp Carolina",
      location: "Brevard, North Carolina, USA",
      link: "http://www.campcarolina.com/",
      duration: "USA Summer 2016",
      description: "Leader of the Mountaineering Team of 10 staff. I was responsible for managing multiple daily trips equipment and personnel for high adventure mountainbiking, rock climbing, caving and mountainboarding. We would take up to 15 children (ages 5-16) on guided mountainbiking, rock climbing, caving and mountainboarding trips. Administering regular medicine and treatment and treating medical emergencies under Wilderness First Responder training. Driving people carrier vans with equipment and overnight load. Completing evaluations for all Mountaineering staff."
    },
    {
      position: "Wilderness First Responder",
      company: "Wilderness Medical Associates",
      location: "Brevard, North Carolina, USA",
      link: "https://www.wildmed.com/",
      duration: "USA Summer 2016",
      description: "This certification is medical training, leadership, and critical thinking for outdoor, low-resource, and remote professionals and leaders. The Wilderness First Responder is the best and most rewarding certification I have completed. It has provided me with invaluable knowledge of medical situations and how to handle them. I have demonstrated many of the practises I learnt both professionally and personally since completing the certification.",
    },
    {
      position: "Content Manager",
      company: "Kogan.com",
      location: "Melbourne, Australia",
      link: "https://www.kogan.com/au/",
      duration: "January 2013 - December 2015",
      description: "At Kogan.com I was the lead of the Kogan.com Content Team, managing over 25,000 products. My role was to ensure the quality of website content in product listings and marketing. I drove and executed marketing campaigns and projects in close collaborations with key internal teams and corporate partners. I designed and developed website and promotional static pages with HTML and CSS, and implemented promotional features with Javascript.",
    },
    {
      position: "Web Developer",
      company: "Freelance Web Development",
      location: "Melbourne, Australia",
      link: "https://www.kogan.com/au/",
      duration: "September 2015 - December 2016",
      description: "I designed and developed solutions for web applications, web shops, online profiles and landing pages. I provided services and assistance with upgrading and improving the reach of existing applications with mobile support, SEO and speed improvements.",
    },
    {
      position: "Computer Technician",
      company: "Pro-Bit Computers",
      location: "Melbourne, Australia",
      link: "http://www.probit.com.au/",
      duration: "September 2010 - December 2012",
      description: "Computer hardware and software diagnostics & repair, operating system diagnostic and repair, VM support, improving system performance (removal of malware, junk files), hardware replacement in desktops and notebooks, system upgrades and new builds, customer service via shopfront and phone, on-site and remote assistance, linux system tools experience, hard drive ghosting/imaging.",
    }
  ]
}
