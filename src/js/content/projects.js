import {Project} from '../Home/Projects'

export default {
  id: 'projects',
  title: 'Projects',
  component: Project,
  items: [
    {
      name: "Ticketbutler Event Management App",
      company: "Ticketbutler",
      contribution: "Native app built in React Native for both iOS and Android.",
      link: "https://itunes.apple.com/ph/app/ticketbutler/id1293446423",
      image: "/img/projects/ticketbutler-app.png",
      description: "The Ticketbutler app enables event organisers to keep up with ticket sales and scan guests' tickets across multiple devices.",
    },
    {
      name: "Delihood Web Application",
      company: "Delihood",
      contribution: "Front-end built with ReactJS and styled-components with server-side rendering.",
      link: "http://delihood.com/",
      image: "/img/projects/delihood.png",
      description: "Delihood connects you with likeminded neighbours who enjoy sharing and trying new foods.",
    },
    {
      name: "BilletFix Event Management App",
      company: "BilletFix",
      contribution: "Native app built in React Native for both iOS and Android.",
      link: "https://itunes.apple.com/dk/app/billetfix/id1281261719",
      image: "/img/projects/billetfix-app.png",
      description: "The BilletFix app enables event organisers to keep up with ticket sales and scan guests' tickets across multiple devices.",
    },
    {
      name: "BilletFix Web Application",
      company: "BilletFix",
      contribution: "Front-end built with ReactJS, SCSS and Django templates.",
      link: "https://billetfix.dk/",
      image: "/img/projects/billetfix.png",
      description: "The BilletFix web application is built on values of simplicity, speed, and easy of use. BilletFix cuts out the unnecessary steps traditional ticketing companies require, and creates a smooth and simple experience for both event organisers and ticker buyers.",
    },
    {
      name: "Guest 'Check-In' App",
      company: "Ticketbutler",
      contribution: "A 'real-time' check-in ReactJS app for event organisers managing large events.",
      link: "https://ticketbutler.io/",
      image: "/img/projects/check-in.png",
      description: "The Ticketbutler check-in app allows many event hosts to simultaneously check-in guests on their own device, and updates their list immediately every time a guest is checked in.",
    },
    {
      name: "BilletFix Blog",
      company: "BilletFix",
      contribution: "Built with Django, SCSS and Webpack 2.",
      link: "https://blog.billetfix.dk/",
      image: "/img/projects/event-guf.png",
      description: "'Event-guf' is a blog for the BilletFix community - both organisers and ticket buyers - to come for information, hot tips and tricks about all things events.",
    },
    {
      name: "[Talk] How to make a tech choice",
      company: "Tiny Rhino",
      contribution: "Provides 'CTO' insights into making good technical descisions for your business.",
      link: "https://tiny-rhino.github.io/how-to-make-a-tech-choice/",
      image: "/img/projects/tech-choice.png",
      description: "The talk takes you through the process of developing product phases - from 'idea' to 'first version' - and how to do it most efficiently and effectively using the right tools.",
    },
    {
      name: "Ticketbutler Landing Page",
      company: "Ticketbutler",
      contribution: "The home of Ticketbutler - enabling businesses to get started with ticketing.",
      link: "https://ticketbutler.io/",
      image: "/img/projects/ticketbutler-landing.png",
      description: "The Ticketbutler landing page is a static site generated with PugJS, SCSS and Webpack 2.",
    }
  ]
}
