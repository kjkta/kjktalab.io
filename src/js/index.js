import React from 'react'
import Template from './Template/'
import Home from './Home/'

import { renderToString } from 'react-dom/server'
import { ServerStyleSheet } from 'styled-components'

export default () => {
  const sheet = new ServerStyleSheet()
  renderToString(sheet.collectStyles(<Template component={<Home />} />))
  const css = sheet.getStyleElement()
  return renderToString(<Template component={<Home />} style={css} />)
}
