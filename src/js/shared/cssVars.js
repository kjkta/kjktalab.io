export default {
  primary: '#216be4',
  primaryRGB: '33, 107, 228',
  grey: '#4a4a4a',
  lightGrey: '#ccc',
  radius: '3px'
}
