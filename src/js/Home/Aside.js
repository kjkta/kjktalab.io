import React from 'react'
import PropTypes from 'prop-types'
import { Aside } from './style'
import { EmHeading, Link, ListItem } from '../Template/style'

export const AsideAbout = ({lists}) =>
  <Aside>
    {lists.map((l, i) => <AsideItem key={i} {...l} />)}
  </Aside>

AsideAbout.propTypes = {
  lists: PropTypes.array.isRequired
}

const AsideItem = ({name, items}) =>
  <div>
    <EmHeading bold>{name}</EmHeading>
    <div>
      {items.map(({ label, link }, i) =>
        <ListItem key={i}>
          {link ?
            <Link href={link} rel='noopener noreferrer' target='_blank'>{label}</Link>
            : label
          }
        </ListItem>
      )}
    </div>
  </div>

AsideItem.propTypes = {
  name: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired
}
