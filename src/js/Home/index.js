import React, { Component } from 'react'

import { HomeImage, Header, HeaderAside, Section, Resume } from './style'
import { Heading, EmHeading, Paragraph } from '../Template/style'

import { AsideAbout } from './Aside'
import { TabMenu } from '../Template/TabMenu'
import content from '../content/'

export default class Home extends Component {
  state = { activeContent: content.find(c => c.id === 'experience') }
  changeContent = id => () => this.setState({activeContent: id})
  render = () => {
    let { activeContent } = this.state
    let [ asideLists, ...resumeCategories ] = content
    return (
      <main id='home'>
        <Header>
          <HeaderAside>
            <EmHeading>Co-Founder & Front-end Lead at Tiny Rhino</EmHeading>
            <Heading>Kyle Thomson</Heading>
            <Paragraph margin='1em 0'>6 years web / native development, content and marketing experience at Kogan.com, BilletFix, Ticketbuter and Tiny Rhino.</Paragraph>
          </HeaderAside>
          <HomeImage src='img/kyle.jpg' alt='Alicante, España' />
        </Header>
        <Section>
          <Paragraph cta center margin='2em 1em'>I'm an Australian young professional with passion for technology and a love for high adventure.</Paragraph>
          <Resume>
            <AsideAbout lists={asideLists} />
            <article>
              <TabMenu tabs={resumeCategories} selected={activeContent.id}
                handleClick={this.changeContent}
              />
              <EmHeading size='1.4em' bold>{activeContent.title}</EmHeading>
              {activeContent.items.map((item, i) =>
                <activeContent.component key={i} {...item} />
              )}
            </article>
          </Resume>
        </Section>
        <script src='/js/Home.js'></script>
      </main>
    )
  }
}
