import styled from 'styled-components'
import vars from '../shared/cssVars'

export const
  Header = styled.header`
    width: 100%;
    height: 100vh;
    overflow: hidden;
    display: flex;
    align-items: center;
    > * { width: 50%; }
  `,

  HomeImage = styled.img`
    height: 100vh;
    object-fit: cover;
  `,

  HeaderAside = styled.aside`
    height: 100vh;
    width: 100%;
    padding: 10em 2em;
    display: flex;
    flex-flow: column;
    justify-content: flex-end;
  `,

  Section = styled.section`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column;
    box-shadow: inset 0 2em 1em -1.8em #eee;
    padding: 4em 2vw;
  `,

  Resume = styled.div`
    display: flex;
    flex-flow: row wrap-reverse;
    align-items: baseline;
    justify-content: space-around;
    width: 100%;
    max-width: 72em;
    margin: 0 auto;
    > * {
      width: 100%;
      max-width: 42em;
      padding: 1em 2em;
    }
  `,

  Aside = styled.aside`
    max-width: 24em;
    > div {
      margin-top: 3em;
    }
  `,

  FeatureItem = styled.div`
    max-width: 72em;
    margin-bottom: 6em;
  `,

  FeatureCard = styled(FeatureItem)`
    border: 1px solid ${vars.lightGrey};
    border-radius: ${vars.radius};
    box-shadow: 1px 1px 1px ${vars.lightGrey};
    img {
      border-top-left-radius: ${vars.radius};
      width: 100%;
      max-height: 25em;
      object-fit: cover;
      object-position: top;
    }
    > div {
      padding: 1em;
      h3 { margin: 1em 0; }
    }
  `
