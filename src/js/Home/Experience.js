import React from 'react'
import PropTypes from 'prop-types'
import { FeatureItem } from './style'
import { Paragraph, Link } from '../Template/style'

export const Experience = ({position, link, company, duration, description}) =>
  <FeatureItem>
    <h3>{position} at <Link href={link} target='_blank' rel='noopener noreferrer'>{company}</Link></h3>
    <em>{duration}</em>
    <Paragraph margin='.5em 0'>{description}</Paragraph>
  </FeatureItem>

Experience.propTypes = {
  position: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  company: PropTypes.string.isRequired,
  duration: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
}
