import React from 'react'
import PropTypes from 'prop-types'
import { FeatureCard } from './style'
import { Paragraph, Link } from '../Template/style'

export const Project = ({name, company, contribution, link, image, description}) =>
  <FeatureCard>
    <a href={link} target='_blank' rel='noopener noreferrer'>
      <img src={image} alt={name} />
    </a>
    <div>
      <h3>{name} | <Link href={link} target='_blank' rel='noopener noreferrer'>{company}</Link>
      </h3>
      <em>{contribution}</em>
      <Paragraph margin='.5em 0'>{description}</Paragraph>
    </div>
  </FeatureCard>

Project.propTypes = {
  name: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  company: PropTypes.string.isRequired,
  contribution: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
}
