export default `
  @font-face {
    font-family: 'Open Sans';
    font-weight: normal;
    src: local('Open Sans Regular'),
       url('/fonts/OpenSans/OpenSans-Regular.woff2') format('woff2'),
       url('/fonts/OpenSans/OpenSans-Regular.ttf') format('truetype');
  }
  @font-face {
    font-family: 'Open Sans';
    font-weight: light;
    src: local('Open Sans Regular'),
       url('/fonts/OpenSans/OpenSans-Light.woff2') format('woff2'),
       url('/fonts/OpenSans/OpenSans-Light.ttf') format('truetype');
  }
  @font-face {
    font-family: 'Open Sans';
    font-weight: bold;
    src: local('Open Sans Regular'),
       url('/fonts/OpenSans/OpenSans-Semibold.woff2') format('woff2'),
       url('/fonts/OpenSans/OpenSans-Semibold.ttf') format('truetype');
  }
  @font-face {
    font-family: 'Martel';
    font-weight: bold;
    src: url('/fonts/Martel/Martel-Heavy.ttf');
  }
`
