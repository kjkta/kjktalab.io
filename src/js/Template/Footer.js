import React from 'react'
import { Paragraph } from './style'

export const Footer = () =>
  <footer>
    <Paragraph center margin='2em 0'>
      This website is built with <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/kjkta/kjkta.gitlab.io/graphs/master/charts'>100% Javascript</a> (ES6+ React & Styled Components compiled to static HTML). Check out <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/kjkta/kjkta.gitlab.io'>the code</a>.
    </Paragraph>
  </footer>
