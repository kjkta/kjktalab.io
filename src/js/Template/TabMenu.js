import React from 'react'
import PropTypes from 'prop-types'
import {Tabs, Tab} from './style'

export const TabMenu = ({ tabs, selected, handleClick }) =>
  <Tabs>
    {tabs.map(t =>
      <Tab key={t.id} selected={selected === t.id} onClick={handleClick(t)}>
        {t.title}
      </Tab>
    )}
  </Tabs>

TabMenu.propTypes = {
  tabs: PropTypes.array.isRequired,
  selected: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired
}
