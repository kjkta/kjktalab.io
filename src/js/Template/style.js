import styled from 'styled-components'
import vars from '../shared/cssVars'

export const
  Body = styled.body`
    margin: 0;
    font-family: 'Open Sans', sans-serif;
    background-color: white;
    *, *:before, *:after {
      box-sizing: border-box;
    }
  `,

  Heading = styled.h1`
    font-family: 'Martel', serif;
    font-size: 3em;
    line-height: 1em;
    margin: .25em 0;a
  `,

  SectionHeading = styled.h2`
    font-family: 'Martel', serif;
    text-transform: uppercase;
    font-size: 1.2em;
  `,

  FeatureList = styled.div`
    display: flex;
  `,

  EmHeading = styled.h3`
    font-weight: ${p => p.bold ? 'bold' : 'normal'};
    text-transform: uppercase;
    font-size: ${p => p.size || '1em'};
    color: ${vars.primary};
  `,

  Paragraph = styled.p`
    color: ${vars.grey};
    margin: ${p => p.margin || 0};
    text-align: ${p => p.center ? 'center' : 'left'};
    line-height: 1.7em;
    font-size: ${p => p.cta ? '1.8em' : '1em'};
  `,

  Link = styled.a`
    color: ${vars.grey};
    text-decoration: none;
    &:after {
      content: '»';
      padding-left: .35em;
      color: ${vars.primary};
      opacity: 0;
      transition: 150ms all;
    }
    &:hover {
      &:after {
        opacity: 1
      }
    }
  `,

  ListItem = styled.span`
    display: block;
    padding: .35em 0;
  `,

  Tabs = styled.div`
    display: flex;
    text-transform: uppercase;
    font-size: .9em;
    font-weight: bold;
    margin-bottom: 4em;
  `,

  Tab = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 1em 0;
    width: 100%;
    max-width: 10em;
    color: ${p => p.selected ? vars.primary : 'initial'};
    border-bottom: ${p => p.selected ? `${vars.primary} 2px solid` : '2px solid transparent'};
    cursor: pointer;
    transition: 200ms all ease-in;
    ${p => !p.selected && `
      &:hover {
        border-bottom: rgba(${vars.primaryRGB}, .1) 2px solid;
      }
    `}
  `
