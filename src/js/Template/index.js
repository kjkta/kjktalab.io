import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import { Body } from './style'
import { Footer } from './Footer'
import fonts from './fonts'

export default class Template extends Component {
  static propTypes = {
    component: PropTypes.object.isRequired,
    style: PropTypes.array
  }
  constructor(props) { super(props) }
  render = () =>
    <html lang='en'>
    <head>
      <meta charSet='utf-8' />
      <title>Kyle Thomson - ReactJS Developer | Copenhagen, DK</title>
      <link rel='icon' href='/img/app-icon-144x144.png'/>
      <meta name='description' content='6 years web / native development, content and marketing experience at Kogan.com, BilletFix, Ticketbuter and Tiny Rhino.' />
      <meta name='viewport' content='width=device-width' />
      <meta name='theme-color' content='#4a4a4a' />
      <meta name='msapplication-navbutton-color' content='#4a4a4a' />
      <meta name='apple-mobile-web-app-status-bar-style' content='#4a4a4a' />
      <style type='text/css' dangerouslySetInnerHTML={{__html: fonts}} />
      {this.props.style}
      <link rel='manifest' href='/manifest.json' />
    </head>
    <Body>
      {this.props.component}
      <Footer />
      <script dangerouslySetInnerHTML={
        {__html: `
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga')

            ga('create', 'UA-70847911-1', 'auto')
            ga('send', 'pageview')
        `}
      } />
      <script dangerouslySetInnerHTML={
        {__html: `
          (function() {
            if('serviceWorker' in navigator) {
              navigator.serviceWorker.register('/service-worker.js');
            }
          })();
        `}
      } />
    </Body>
  </html>
}
