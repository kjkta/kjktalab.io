const path = require('path');

/*
 * __dirname is changed after webpack-ed to another directory
 * so process.cwd() is used instead to determine the correct base directory
 * Read more: https://nodejs.org/api/process.html#process_process_cwd
 */
const CURRENT_WORKING_DIR = process.cwd();

module.exports = {
  root: path.resolve(CURRENT_WORKING_DIR),
  src: path.resolve(CURRENT_WORKING_DIR, 'src'),
  dist: path.resolve(CURRENT_WORKING_DIR, 'dist'),
  public: 'https://kylet.xyz/'
}
