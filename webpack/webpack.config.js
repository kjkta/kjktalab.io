const path = require('path'),
  rules = require('./rules/index'),
  paths = require('./paths'),
  CopyWebpackPlugin = require('copy-webpack-plugin'),
  StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin'),
  SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')

module.exports = {
  context: path.root,

  entry: {
    main: './src/js/',
    Home: './src/js/Home'
  },

  output: {
    path: paths.dist,
    filename: 'js/[name].js',
    publicPath: paths.public,
    libraryTarget: 'umd'
  },

  module: { rules },

  plugins: [
    new CopyWebpackPlugin([
      { from: 'src/img', to: 'img' },
      { from: 'src/fonts', to: 'fonts' },
      { from: 'src/manifest.json' }
    ]),
    new StaticSiteGeneratorPlugin({
      entry: 'main',
      paths: ['/']
    }),
    new SWPrecacheWebpackPlugin(
      {
        cacheId: 'kyle-thomson',
        dontCacheBustUrlsMatching: /\.\w{8}\./,
        filename: 'service-worker.js',
        minify: true,
        navigateFallback: paths.public + 'index.html',
        staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
      }
    ),
  ],

  resolve: {
    descriptionFiles: ['package.json'],
    extensions: ['.js', '.json', '.yml'],
  }
}
