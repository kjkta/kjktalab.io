const paths = require('../paths')

module.exports = {
  test: /\.js$/,

  include: [paths.src],

  use: [
    {
      loader: 'babel-loader',
      options: { presets: ['react', 'es2015', 'stage-0'] }
    }
  ],
}
